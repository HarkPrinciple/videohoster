using System.Collections.Generic;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Linq;
using System;

using Hark.Cloud.Client;
using HarkLib.Core;

namespace Hark.Cloud.Extension
{
    using Client = Hark.Cloud.Client.Client;
    
    public class ClientExtension : IClientSessionExtension
    {
        public override IDictionary<string, string> Help
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    { "search [<name...>]", "List all or search for entries" },
                    { "count [<name...>]", "Count entries" },
                    { "pull <name...>", "Download a matching entry" },
                    { "push <files-path> [<name:qxml> [<voice:qxml>]]", "Upload one or many files" },
                    { "info <name...>", "Get information of an entry" },
                    { "id <name...>", "Get the id of an entry" },
                    { "remove", "Remove an entry without file deletion" },
                    { "delete", "Delete an entry and its files" }
                };
            }
        }

        private string GetId(Client client, Dictionary<string, string> das)
        {
            XElement element = client.SendQuery("video/search/name=" + das["name"]);
            
            int nb = element.Elements().Count();
            if(nb <= 0)
                throw new Exception("Can't find the entry.");
            if(nb > 1)
                throw new Exception("Found " + nb + " entries. Only one must be found.");

            return element.XPathSelectElement("/entry/id").Value;
        }

        public override void LoadCommands(Client client, ConsoleManager.Arguments args)
        {
            args
            .ExtractCommand("video/search/{?name...}", (cas, das) =>
            {
                if(das.ContainsKey("name"))
                    client.Query("video/search/name=" + das["name"]);
                else
                    client.Query("video/search");
            })
            .ExtractCommand("video/count/{?name...}", (cas, das) =>
            {
                if(das.ContainsKey("name"))
                    client.QueryGet(".", "video/count/name=" + das["name"]);
                else
                    client.QueryGet(".", "video/count");
            })
            .ExtractCommand("video/id/name...}", (cas, das) =>
            {
                Console.WriteLine(GetId(client, das));
            })
            .ExtractCommand("video/pull/{name...}", (cas, das) =>
            {
                client.Download(GetId(client, das));
            })
            .ExtractCommand("video/info/{name...}", (cas, das) =>
            {
                client.Info(GetId(client, das));
            })
            .ExtractCommand("video/push/{file}/{?name}/{?voice}", (cas, das) =>
            {
                client.Upload(das["file"], "id", "video/reserve/name=" + das.GetOr("name", "{{name}}") + ";voice=" + das.GetOr("voice", "{{voice}}"), cas.GetKey("tcp"));
            })
            .ExtractCommand("video/remove/{name...}", (cas, das) =>
            {
                string nb = client.SendQuery("video/remove/id=" + GetId(client, das)).XPathSelectElement("count").Value;
                Console.WriteLine(" [o] Removed {0} entries.", nb);
            })
            .ExtractCommand("video/delete/{name...}", (cas, das) =>
            {
                string nb = client.SendQuery("video/delete/id=" + GetId(client, das)).XPathSelectElement("count").Value;
                Console.WriteLine(" [o] Deleted {0} entries and files.", nb);
            });
        }
    }
}