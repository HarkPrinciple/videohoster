using System.Collections.Generic;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Linq;
using System.Net;
using System.IO;
using System;

using Hark.Cloud.Server;

namespace Hark.Cloud.Extension
{
    public class ServerExtension : IServerSessionExtension
    {
        public override string CommandName
        {
            get
            {
                return "video";
            }
        }

        public override bool IsStateful
        {
            get
            {
                return false;
            }
        }

        public string CloudName
        {
            get
            {
                return "videohoster.entries";
            }
        }

        private List<Entry> _Entries = null;
        public List<Entry> GetEntries(Session session)
        {
            if(_Entries == null)
            {
                CloudFile cf = session.CloudFileFromAlias(CloudName);
                if(!cf.Exists)
                    return _Entries = new List<Entry>();

                _Entries = XElement
                    .Parse(cf.ReadAll())
                    .XPathSelectElements("entry")
                    .Select(e => new Entry(e))
                    .ToList();
            }

            return _Entries;
        }

        public void SaveEntries(Session session)
        {
            if(_Entries == null)
                return;
            
            XElement entries = new XElement("entries",
                _Entries
                    .Select(e => e.ToElement("entry"))
                    .ToList()
            );

            CloudFile cf = session.CloudFileFromAlias(CloudName);
            if(!cf.Exists)
                if(!cf.Reserve())
                    throw new CloudMessageException(Session.GetError(13, "Couldn't save entries."));

            cf.WriteAll(entries.ToString(SaveOptions.DisableFormatting));
        }

        public override XElement Compute(Session session, IPEndPoint sender, XElement doc)
        {
            try
            {
            XElement xcmd = doc.XPathSelectElement("/*");
            if(xcmd == null)
                return new XElement(CommandName,
                    HelpElement.CreateHelpElement(new HelpElement[]
                    {
                        new HelpElement("reserve", desc : "Reserve files for an entry", format : "reserve/name={name};voice={voice}"),
                        new HelpElement("search", desc : "Search for one or many entries", format : "search/[name={name};][voice={voice}]"),
                        new HelpElement("count", desc : "Count the number of matching entries", format : "count/[name={name};][voice={voice}]"),
                        new HelpElement("put", desc : "Create an entry without reservation", format : "put/id={id};name={name};voice={voice}"),
                        new HelpElement("remove", desc : "Remove an entry without file deletion", format : "remove/id={id}"),
                        new HelpElement("delete", desc : "Delete an entry and its files", format : "delete/id={id}")
                    })
                );

            string cmd = xcmd.Name.LocalName.Trim().ToLower();
            switch(cmd)
            {
                case "reserve":
                    return Reserve(session, xcmd);

                case "search":
                    return Search(session, xcmd);

                case "count":
                    return Count(session, xcmd);

                case "put":
                    return Put(session, xcmd);

                case "remove":
                    return Remove(session, xcmd);

                case "delete":
                    return Delete(session, xcmd);
                
                default:
                    return Session.GetError(1, "Can't understand the command \"" + cmd + "\"");
            }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        private XElement Remove(Session session, XElement doc)
        {
            string id = Session.GetOrThrow(doc, "id");

            int nb = GetEntries(session).RemoveAll(e => e.Id == id);

            SaveEntries(session);

            return new XElement("remove",
                new XElement("count", nb)
            );
        }

        private XElement Delete(Session session, XElement doc)
        {
            string id = Session.GetOrThrow(doc, "id");

            List<Entry> entries = GetEntries(session);
            
            int nb = 0;
            foreach(var e in entries.Where(e => e.Id == id))
            {
                entries.Remove(e);
                ++nb;
            }

            if(nb > 0)
                session.CloudFileFromId(id).Delete();

            SaveEntries(session);

            return new XElement("delete",
                new XElement("count", nb)
            );
        }

        private XElement Put(Session session, XElement doc)
        {
            Entry element = new Entry()
            {
                Id = Session.GetOrThrow(doc, "id"),
                Name = Session.GetOrThrow(doc, "name"),
                Voice = Session.GetOrThrow(doc, "voice")
            };

            GetEntries(session).Add(element);

            SaveEntries(session);

            return element.ToElement("create");
        }

        private XElement Reserve(Session session, XElement doc)
        {
            string name = Session.GetOrThrow(doc, "name");
            string voice = Session.GetOrThrow(doc, "voice");

            CloudFile cf = session.CloudFileFromId();
            cf.Reserve();

            Entry element = new Entry()
            {
                Id = cf.OriginalName,
                Name = name,
                Voice = voice
            };

            GetEntries(session).Add(element);

            SaveEntries(session);

            return element.ToElement("reserve");
        }

        private XElement Search(Session session, XElement doc)
        {
            XElement xname = doc.XPathSelectElement("name");
            XElement xvoice = doc.XPathSelectElement("voice");

            string[] name = xname == null ? new string[0] : xname.Value.Trim().ToLower().Split();
            string[] voice = xvoice == null ? new string[0] : xvoice.Value.Trim().ToLower().Split();

            return new XElement("search",
                GetEntries(session)
                    .Where(e => name.All(v => e.Name.ToLower().Contains(v)))
                    .Where(e => voice.All(v => e.Voice.ToLower().Contains(v)))
                    .Select(e => e.ToElement("entry")).ToList()
            );
        }

        private XElement Count(Session session, XElement doc)
        {
            int nb = Search(session, doc)
                .Elements()
                .Count();
            
            return new XElement("count", nb);
        }
    }
}