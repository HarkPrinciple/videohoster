using System.Xml.XPath;
using System.Xml.Linq;

namespace Hark.Cloud.Extension
{
    public class Entry
    {
        public Entry()
        { }
        public Entry(XElement e)
        {
            Id = e.XPathSelectElement("id").Value;
            Name = e.XPathSelectElement("name").Value;
            Voice = e.XPathSelectElement("voice").Value;
        }

        public string Id
        {
            get;
            set;
        }
        
        public string Name
        {
            get;
            set;
        }
        
        public string Voice
        {
            get;
            set;
        }

        public XElement ToElement(string name = "entry")
        {
            return new XElement(name,
                new XElement("id", Id),
                new XElement("name", Name),
                new XElement("voice", Voice)
            );
        }
    }
}